gcc -o dnsdrdos dnsdrdos.c -s

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Compile dnsdrdos... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf dnsdrdos /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
